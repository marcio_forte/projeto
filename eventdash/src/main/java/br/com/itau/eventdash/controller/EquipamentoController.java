package br.com.itau.eventdash.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import br.com.itau.eventdash.dao.EquipamentoDAO;
import br.com.itau.eventdash.model.Equipamento;

@CrossOrigin(origins = "*")
@RestController
public class EquipamentoController {
    
    @Autowired
    private EquipamentoDAO dao;

    
    @PostMapping("/equipamento")
    public ResponseEntity<Equipamento> listaEquipamento(@RequestBody Equipamento equipamentoPesq){
        Equipamento equipamento = dao.findById(equipamentoPesq.getId()).orElse(null);

        if (equipamento != null) {
            return ResponseEntity.ok(equipamento);
        } else {
            return ResponseEntity.status(404).build();
        }
    } 

    @GetMapping("/equipamento/{codigo}")
    public ResponseEntity<Equipamento> listaEquipamento(@PathVariable int codigo){
        Equipamento equipamento = dao.findById(codigo).orElse(null);

        if (equipamento != null) {
            return ResponseEntity.ok(equipamento);
        } else {
            return ResponseEntity.status(404).build();
        }
    }


}
